package com.cppdesign.helloworld;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldUnitTest {
    private HelloWorld helloWorld;

    @Test
    public void toString_correct() {
        helloWorld = new HelloWorld("Steven");
        assertEquals("Hello, Steven!", helloWorld.toString());
    }

    // it must be fail because the sentence is missing one symbol (!)
    @Test
    public void toString_fail() {
        helloWorld = new HelloWorld("Steven");
        assertEquals("Hello, Steven", helloWorld.toString());
    }
}
