package com.cppdesign.helloworld;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

public class HelloWorldInstrumentedTest {
    Context appContext;

    @Before
    public void createInstrumentationRegistry() {
        appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @Test
    public void useAppName() {
        assertEquals("Hello World!", appContext.getApplicationInfo().loadLabel(appContext.getPackageManager()).toString());
    }

    @Test
    public void useAppNameFail() {
        assertEquals("Hello, World!", appContext.getApplicationInfo().loadLabel(appContext.getPackageManager()).toString());
    }
}
