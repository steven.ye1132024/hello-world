package com.cppdesign.helloworld

class HelloWorld(private val name: String) {
    override fun toString(): String {
        return "Hello, $name!"
    }
}